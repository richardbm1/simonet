from django.contrib import admin
from curso.models import Curso, Materia, MateriaCurso, TareasYEvaluaciones
# Register your models here.


admin.site.register(Curso)
admin.site.register(Materia)
admin.site.register(MateriaCurso)
admin.site.register(TareasYEvaluaciones)
