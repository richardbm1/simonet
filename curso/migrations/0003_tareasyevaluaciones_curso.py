# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-29 17:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('curso', '0002_auto_20160729_1733'),
    ]

    operations = [
        migrations.AddField(
            model_name='tareasyevaluaciones',
            name='curso',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='curso.Curso'),
            preserve_default=False,
        ),
    ]
