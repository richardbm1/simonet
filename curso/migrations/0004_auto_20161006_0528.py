# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-10-06 05:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('curso', '0003_tareasyevaluaciones_curso'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='materiacurso',
            name='hora_fin',
        ),
        migrations.RemoveField(
            model_name='materiacurso',
            name='hora_inicio',
        ),
        migrations.AddField(
            model_name='materiacurso',
            name='horario',
            field=models.IntegerField(choices=[(0, '7:00 am - 9:00 am'), (1, '10:00 am - 12:00 m'), (3, '1:00 pm - 3:00 pm'), (4, '4:00 pm - 6:00 pm')], default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='materiacurso',
            name='curso',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='materia', to='curso.Curso'),
        ),
    ]
