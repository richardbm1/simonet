from __future__ import unicode_literals
from django.db import models

# Create your models here.

class Curso(models.Model):
	ANIO=(
		('1','1'),
		('2','2'),
		('3','3'),
		('4','4'),
		('5','5'),
		('6','6'),
	)
	SECCION=(
		('A','A'),
		('B','B'),
		('C','C'),
		('D','D'),
		('E','E'),
		('F','F'),
	)
	anio=models.CharField(choices=ANIO, max_length=1)
	seccion=models.CharField(choices=SECCION, max_length=1)

	def get_curso(self):
		return self.anio+self.seccion

	def __unicode__(self):
		return self.get_curso()


class Materia(models.Model):
	materia=models.CharField(max_length=25)
	

	def __unicode__(self):
		return self.materia

HORARIO=(
	(1,'7:00 am - 9:00 am'),
	(2,'10:00 am - 12:00 m'),
	(3,'1:00 pm - 3:00 pm'),
	(4,'4:00 pm - 6:00 pm'),
)
class MateriaCurso(models.Model):
	curso=models.ForeignKey(Curso,related_name="materia")
	materia=models.ForeignKey(Materia)
	lunes=models.BooleanField()
	martes=models.BooleanField()
	miercoles=models.BooleanField()
	jueves=models.BooleanField()
	viernes=models.BooleanField()
	horario=models.IntegerField(choices=HORARIO)

	class Meta:		
		verbose_name = 'Materia del curso'
		verbose_name_plural = 'Materias del curso'
		ordering=('horario',)

	def __unicode__(self):
		return self.curso.get_curso()+" "+self.materia.materia

class TareasYEvaluaciones(models.Model):
	TIPO=(
		('TA','Tarea'),
		('EX','Examen'),
	)
	"""tareas pendientes de los alumnos"""
	curso=models.ForeignKey(Curso,related_name="tarea")
	materia=models.ForeignKey(MateriaCurso)
	tipo=models.CharField(choices=TIPO,max_length=2)
	fecha=models.DateTimeField()
	tarea=models.TextField()

	class Meta:		
		verbose_name = 'Tarea o Evaluacion'
		verbose_name_plural = 'Tareas y Evaluaciones'
	
	def __unicode__(self):
		return self.curso.get_curso()+"-"+self.get_tipo_display()+" de "+self.materia.materia.materia


		