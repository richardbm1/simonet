from django.views.generic import TemplateView, ListView, DetailView
from accounts.models import Alumno, Representante, Lapso
from calendario.models import Citaciones, Reuniones,Reportes
from curso.models import MateriaCurso,TareasYEvaluaciones

class InicioView(ListView):
	template_name="Inicio.html"
	model=Alumno
	def get_queryset(self):
		queryset=super(InicioView,self).get_queryset()
		return queryset.filter(representante=self.request.user)

class BoletinesView(ListView):
	template_name="Boletines2.html"
	model=Alumno
	def get_queryset(self):
		queryset=super(BoletinesView,self).get_queryset()
		return queryset.filter(representante=self.request.user)
	
class CalendarioView(ListView):
	template_name="Calendario.html"
	model=Citaciones
	def get_context_data(self,*args,**kwargs):
		context=super(CalendarioView,self).get_context_data(*args,**kwargs)
		context['alumnos']=Alumno.objects.filter(representante=self.request.user)
		return context
	def get_queryset(self):
		queryset=super(CalendarioView,self).get_queryset()
		return queryset.filter(representante=self.request.user)
	


class ReportesView(ListView):
	template_name="Reporte2.html"
	model=Alumno
	def get_queryset(self):
		queryset=super(ReportesView,self).get_queryset()
		return queryset.filter(representante=self.request.user)

	
class CitacionDetailView(DetailView):
	template_name="Citaciones.html"
	model=Citaciones

class TareaDetailView(DetailView):
	template_name="Tareas.html"
	model=TareasYEvaluaciones

