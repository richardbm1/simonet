from django.views.generic import TemplateView

class Template(TemplateView):
	def get_context_data(self,**kwargs):
		context=super(Template,self).get_context_data(**kwargs)
		self.template_name=self.kwargs['slug']+".html"
		return context

class InicioView(TemplateView):
	template_name="Inicio.html"

class BoletinesView(TemplateView):
	template_name="Boletines.html"

class CalendarioView(TemplateView):
	template_name="Calendario.html"
	
class CitacionesView(TemplateView):
	template_name="Citaciones.html"

class ReportesView(TemplateView):
	template_name="Reporte.html"


