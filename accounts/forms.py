# -*- coding: utf-8 -*-
from django.contrib.auth.forms import (
    UserChangeForm,
    UserCreationForm
)

from accounts.models import Representante


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Representante


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = Representante