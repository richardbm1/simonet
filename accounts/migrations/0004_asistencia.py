# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-29 18:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20160729_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='Asistencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dia', models.DateField()),
                ('alumno', models.ManyToManyField(to='accounts.Alumno')),
            ],
        ),
    ]
