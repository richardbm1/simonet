from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models
from curso.models import Curso

# Create your models here.



class Representante(AbstractUser):
	cedula=models.IntegerField(null=True)
	
	class Meta:		
		verbose_name = 'Representante'
		verbose_name_plural = 'Representantes'
	def get_name_or_username(self):
		if self.first_name:
			return self.first_name+" "+self.last_name
		else:
			return self.username
	def __unicode__(self):
		return self.get_name_or_username()

class Alumno(models.Model):
	nombre=models.CharField(max_length=20)
	apellido=models.CharField(max_length=20)
	correo=models.EmailField(blank=True)
	edad=models.IntegerField()
	curso=models.ForeignKey(Curso)
	representante=models.ForeignKey(Representante)

	def __unicode__(self):
		return self.nombre+" "+self.apellido

class Lapso(models.Model):
	"""Lapso para entrega de notas"""
	LAPSO=(
		(1,'Primero'),
		(2,'Segundo'),
		(3,'Tercero'),
	)
	CALIFICACION=(
		('A','A'),
		('B','B'),
		('C','C'),
		('D','D'),
		('E','E'),
	)	
	alumno=models.ForeignKey(Alumno)
	lapso=models.IntegerField(choices=LAPSO)
	analisis=models.TextField()
	calificacion=models.CharField(choices=CALIFICACION, max_length=1)
	def __unicode__(self):
		return self.get_lapso_display()
		
class Asistencia(models.Model):
	alumno=models.ManyToManyField(Alumno)
	dia=models.DateField()
	curso=models.ForeignKey(Curso)
	def __unicode__(self):
		return str(self.dia)+" - "+self.curso.get_curso()

