from django.contrib import admin
from accounts.models import Representante, Alumno, Asistencia,Lapso
from django.contrib.auth.admin import UserAdmin
from accounts.forms import CustomUserChangeForm,CustomUserCreationForm

class AlumnoInline(admin.StackedInline):
	model = Alumno
	extra = 0
	fieldsets =  (
		(
			'Informacion Personal', {
				'fields': (
					'nombre',
					'apellido',
					'edad',
					'correo',
					'curso',
					
				)
			}
		),
		
	)
class LapsoInline(admin.StackedInline):
	model = Lapso
	extra = 0
	fieldsets =  (
		(
			'', {
				'fields': (
					'lapso',
					'analisis',
					'calificacion',
					
				)
			}
		),
		
	)

class AlumnoAdmin(admin.ModelAdmin):
	model=Alumno
	inlines =[LapsoInline]

class RepresentanteAdmin(UserAdmin):
	list_display = ('cedula', 'first_name','last_name',)
	form = CustomUserChangeForm
	add_form = CustomUserCreationForm
	search_fields=('cedula','first_name','last_name')
	inlines = [AlumnoInline]
	fieldsets =  (
		(
			'Informacion Personal', {
				'fields': (
					'username',
					'password',
					'first_name',
					'last_name',
					'email',
					'cedula',
					
				)
			}
		),
		
	)

class AsistenciaAdmin(admin.ModelAdmin):
	model = Asistencia
	filter_horizontal=['alumno']

class LapsoAdmin(admin.ModelAdmin):
	model = Lapso
	list_display = ("lapso",'get_nombre','get_apellido','get_curso')
	search_fields = ("lapso",'get_nombre','get_apellido','get_curso')
	list_filter = ("lapso",)

	def get_nombre(self,obj):
		return obj.alumno.nombre
	def get_apellido(self,obj):
		return obj.alumno.apellido
	def get_curso(self,obj):
		return obj.alumno.curso.get_curso()
	get_nombre.short_description = "Nombre"
	get_apellido.short_description = "Apellido"
	get_curso.short_description = "Curso"

	get_nombre.admin_order_field = 'alumno__nombre'
	get_apellido.admin_order_field = 'alumno__apellido'
	get_curso.admin_order_field = 'alumno__curso'

	



admin.site.register(Representante,RepresentanteAdmin)
admin.site.register(Alumno, AlumnoAdmin)
admin.site.register(Asistencia,AsistenciaAdmin)
admin.site.register(Lapso,LapsoAdmin)