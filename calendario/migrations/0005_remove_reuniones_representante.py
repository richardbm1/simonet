# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-09-03 18:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calendario', '0004_reuniones_representante'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reuniones',
            name='representante',
        ),
    ]
