# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-29 17:07
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('calendario', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='reuniones',
            options={'verbose_name': 'Reunion', 'verbose_name_plural': 'Reuniones'},
        ),
        migrations.AddField(
            model_name='reuniones',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 29, 17, 7, 29, 993001, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
