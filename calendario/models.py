from __future__ import unicode_literals
from datetime import datetime
from django.db import models
from accounts.models import Representante

# Create your models here.
class Reuniones(models.Model):
	motivo=models.TextField()
	fecha=models.DateTimeField()

	class Meta:
		verbose_name='Reunion'
		verbose_name_plural='Reuniones'

	def __unicode__(self):
		return str(self.fecha)

class Citaciones(models.Model):
	motivo=models.TextField()
	fecha=models.DateTimeField()
	representante=models.ForeignKey(Representante)


	class Meta:
		verbose_name='Citacion'
		verbose_name_plural='Citaciones'

	def __unicode__(self):
		return str(self.fecha)